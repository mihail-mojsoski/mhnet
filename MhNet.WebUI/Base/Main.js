const socket = new WebSocket('ws://'+document.location.host+'/event', 'mh');
const protocol = {};
let done = false;
function LoadScript(source)  {
    let script = document.createElement("script");
    script.src = source;
    document.body.appendChild(script);
}

function stringifyEvent(e) {
    const obj = {};
    for (let k in e) {
        obj[k] = e[k];
    }
    return JSON.stringify(obj, (k, v) => {
        if (v instanceof Node) return 'Node';
        if (v instanceof Window) return 'Window';
        return v;
    });
}
function Send(command, data) {
    let obj = {Command: command, Data: data};
    socket.send(JSON.stringify(obj));
}

function Report(title, message) {
    done = true;
    socket.close();
    document.head.innerHTML = '<meta charset="UTF-8"><link rel="stylesheet" href="/Base/Normal.css"><style>body { background: darkred; }</style>';
    document.body.innerHTML = '<h1><span id="error">ERROR:</span>'+title+'</h1>\n<textarea disabled readonly="readonly" id="exception"></textarea>';
    let exception = document.getElementById('exception');
    exception.textContent = message;
    exception.rows = 50;
    document.title = title;
}

socket.addEventListener('open', function (_) {
    Send('IsServerOk', null);
});

socket.addEventListener('message', function (event) {
    let message = JSON.parse(event.data);
    protocol[message.Command](message.Data);
});

socket.addEventListener('error', function (data) {
    Report('WebSocket Error', "Got an error event from WebSocket: " + stringifyEvent(data));
});

socket.addEventListener('close', function (data) {
    if (!done) {
        Report('WebSocket Closed', "The connection to the WebSocket was closed: " + stringifyEvent(data));
    }
});

LoadScript("/WebProtocol/Protocol.js");