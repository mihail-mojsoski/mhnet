namespace MhNet.WebUI.UI
{
    public enum UiMethod
    {
        RegisterDocumentEvent,
        RegisterNodeEvent,
        GetNodeProperty,
        SetNodeProperty
    }
}