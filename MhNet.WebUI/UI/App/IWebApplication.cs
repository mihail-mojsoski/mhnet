using System.Collections.Generic;
using System.Threading.Tasks;
using MhNet.Common;
using MhNet.WebUI.UI.Events;
using MhNet.WebUI.WebProtocol.Server;

namespace MhNet.WebUI.UI.App
{
    public interface IWebApplication<T> where T : IWebApplication<T>, new()
    {
        Task Init(ProtocolManager<T> parent, Protocol<T> protocol);
        Task HandleEvent(EventPoint eventPoint, object data);
        Dictionary<string, Request> PropertyRequests { get; }
        void Stop();
    }
}