using System;
using System.IO;
using System.Threading.Tasks;
using MhNet.Common;
using MhNet.WebUI.UI.Events;
using MhNet.WebUI.UI.Nodes;
using MhNet.WebUI.WebProtocol;
using MhNet.WebUI.WebProtocol.Server;
using Newtonsoft.Json.Linq;

namespace MhNet.WebUI.UI.App
{
    public class WebApplicationProtocol<T> : Protocol<T> where T : IWebApplication<T>, new()
    {
        public WebApplicationProtocol() : base(new()
        {
            {"EventCallback", EventCallback},
            {"NodeProperty", NodeProperty}
        })
        {
            Commands.Add("Ready", Ready);
        }

        private static async Task EventCallback(ProtocolManager<T> manager, ProtocolMessage message)
        {
            var args = JObject.Parse(message.Data);
            var info = args["EventInfo"];
            if (info == null || !Enum.TryParse<EventTarget>(info["Target"]?.ToString(), out var target))
            {
                throw new InvalidDataException(nameof(target));
            }
            if (!Enum.TryParse<Event>(info["Event"]?.ToString(), true, out var jsEventType))
            {
                throw new InvalidDataException(nameof(target));
            }
            string id = null;
            if (target == EventTarget.Node)
            {
                id = info["Id"]?.ToString();
            }

            var jsEvent = new EventPoint(id, target, jsEventType);
            await manager.Instance.HandleEvent(jsEvent, args);
        }
        private static Task NodeProperty(ProtocolManager<T> manager, ProtocolMessage message)
        {
            try
            {
                var data = JObject.Parse(message.Data);
                var id = data["Id"]?.ToString();
                var value = data["Value"]?.ToString();
                var key = data["Key"]?.ToString();
                manager.Instance.PropertyRequests[id ?? string.Empty].Complete(new SetPropertyRequest(id, key, value));
                return Task.CompletedTask;
            }
            catch (Exception e)
            {
                return Task.FromException(e);
            }

        }
        public async Task SetNodeProperty(ProtocolManager<T> parent, SetPropertyRequest request)
        {
            await parent.Send(UiMethod.SetNodeProperty, request);
        }
        public async Task<SetPropertyRequest> GetNodeProperty(ProtocolManager<T> parent, GetPropertyRequest request)
        {
            var req = new Request();
            parent.Instance.PropertyRequests.Add(request.Id, req);
            await parent.Send(UiMethod.GetNodeProperty, request);
            var result = await req.GetResult<SetPropertyRequest>();
            parent.Instance.PropertyRequests.Remove(request.Id);
            return result;
        }

        public async Task ElementSet(ProtocolManager<T> parent, TargetRequest request)
        {
            await parent.Send(ProtocolMethod.ElementSet, request);
        }
        public async Task ElementInsert(ProtocolManager<T> parent, TargetRequest request)
        {
            await parent.Send(ProtocolMethod.ElementInsert, request);
        }
        public async Task RegisterEvent(ProtocolManager<T> parent, EventRegistrationRequest eventRegistrationRequest)
        {
            if (eventRegistrationRequest.GetType() == typeof(NodeEventRequest))
            {
                await parent.Send(UiMethod.RegisterNodeEvent, eventRegistrationRequest);
            }
            if (eventRegistrationRequest.GetType() == typeof(DocumentEventRequest))
            {
                await parent.Send(UiMethod.RegisterDocumentEvent, eventRegistrationRequest);
            }
        }
        private async Task Ready(ProtocolManager<T> parent, ProtocolMessage message)
        {
            await parent.Instance.Init(parent, this);
        }

        public override void Init(ProtocolServer<T> server)
        {
            server.Router.FromAssembly(typeof(T).Assembly, "/");
        }

        protected override async Task Bind(ProtocolManager<T> parent)
        {
            await parent.Send(ProtocolMethod.LoadScript, new ProtocolData($"/{nameof(UI)}/Protocol.js"));
        }
        public static async Task Start(string prefix)
        {
            await new ProtocolServer<T>(new WebApplicationProtocol<T>()).Start(prefix);
        }
    }
}