using System;

namespace MhNet.WebUI.UI.App
{
    public class ClientException : Exception
    {
        public ClientException(string str) : base($"A client-side problem has been reported: {str}")
        {
        }
    }
}