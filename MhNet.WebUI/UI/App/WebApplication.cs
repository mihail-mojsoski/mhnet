using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MhNet.Common;
using MhNet.WebUI.UI.Events;
using MhNet.WebUI.UI.Nodes;
using MhNet.WebUI.WebProtocol.Server;

namespace MhNet.WebUI.UI.App
{
    public abstract class WebApplication<T> : IWebApplication<T> where T : IWebApplication<T>, new()
    {
        protected abstract Task Request();
        public ProtocolManager<T> Parent;
        public WebApplicationProtocol<T> Protocol;
        public readonly Dictionary<EventPoint, Action<object>> Events = new();
        public Dictionary<string, Request> PropertyRequests { get; } = new();
        public INode Head { get; private set; }
        public INode Body { get; private set; }
        public Task<Node<T>> CreateNode(string type, string value = "", INode parent = null, string id = null)
        {
            var node = new Node<T>(this, type, value, parent, id);
            return Task.FromResult(node);
        }
        public async Task Init(ProtocolManager<T> parent, Protocol<T> protocol)
        {
            Parent = parent;
            Protocol = protocol as WebApplicationProtocol<T>;
            Head = new Head<T>(this);
            Body = new Body<T>(this);
            await RegisterEvent(new DocumentEventRequest(Event.Error), o => throw new ClientException(o.ToString()));
            await Request();
        }

        internal async Task SetAttribute(string id, string key, string value)
        {
            await Protocol.SetNodeProperty(Parent, new(id, key, value));
        }
        
        internal async Task<SetPropertyRequest> GetAttribute(string id, string key)
        {
            return await Protocol.GetNodeProperty(Parent, new(id, key));
        }

        internal async Task RegisterEvent(EventRegistrationRequest eventRegistrationRequest, Action<object> callback)
        {
            var jsEvent = eventRegistrationRequest.ToJsEvent();
            if (Events.ContainsKey(jsEvent))
            {
                Events[jsEvent] += callback;
            }
            else
            {
                Events.Add(jsEvent, callback);
            }
            await Protocol.RegisterEvent(Parent,eventRegistrationRequest);
        }

        public Task HandleEvent(EventPoint eventPoint, object data)
        {
            if (!Events.ContainsKey(eventPoint))
            {
                return Task.FromException(new InvalidOperationException());
            }
            Events[eventPoint](data);
            return Task.CompletedTask;
        }

        public virtual void Stop()
        {
            
        }
    }
}