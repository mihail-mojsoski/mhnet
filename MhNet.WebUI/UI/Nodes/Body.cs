using System.IO;
using System.Text;
using MhNet.WebUI.UI.App;

namespace MhNet.WebUI.UI.Nodes
{
    public class Body<T> : TextWriter, INode where T : IWebApplication<T>, new()
    {
        private readonly WebApplication<T> _webApplication;
        public Body(WebApplication<T> webApplication)
        {
            _webApplication = webApplication;
            Encoding = Encoding.Default;
        }

        public override void Write(char value)
        {
            var append = _webApplication.Protocol.ElementInsert(_webApplication.Parent, new("Body", null, value.ToString()));
            append.Wait();
        }

        public override void Write(string value)
        {
            var append = _webApplication.Protocol.ElementInsert(_webApplication.Parent, new("Body", null, value));
            append.Wait();
        }
        public override Encoding Encoding { get; }
        public TextWriter Writer => this;
        public string Id => null;
    }
}