using System;
using MhNet.WebUI.WebProtocol.Server;
using Newtonsoft.Json;

namespace MhNet.WebUI.UI.Nodes
{
    [Serializable]
    public record GetPropertyRequest(string Id, string Key) : ProtocolData;
}