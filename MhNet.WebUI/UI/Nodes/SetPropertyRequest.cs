using System;
using MhNet.WebUI.WebProtocol.Server;
using Newtonsoft.Json;

namespace MhNet.WebUI.UI.Nodes
{
    [Serializable]
    public record SetPropertyRequest(string Id, string Key, string Value) : ProtocolData;
}