using System.IO;

namespace MhNet.WebUI.UI.Nodes
{
    public interface INode
    {
        TextWriter Writer { get; }
        string Id { get; }
    }
}