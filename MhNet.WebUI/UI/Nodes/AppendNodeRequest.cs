using MhNet.WebUI.WebProtocol.Server;

namespace MhNet.WebUI.UI.Nodes
{
    public record AppendNodeRequest(string Id, string Html) : ProtocolData;
}