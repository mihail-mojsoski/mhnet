using System.IO;
using System.Text;
using MhNet.WebUI.UI.App;

namespace MhNet.WebUI.UI.Nodes
{
    public class Head<T> : TextWriter, INode where T : IWebApplication<T>, new()
    {
        private readonly WebApplication<T> _webApplication;
        public Head(WebApplication<T> webApplication)
        {
            _webApplication = webApplication;
            Encoding = Encoding.Default;
        }

        public override void Write(char value)
        {
            var append = _webApplication.Protocol.ElementInsert(_webApplication.Parent, new("Head", null, value.ToString()));
            append.Wait();
        }

        public override void Write(string value)
        {
            var append = _webApplication.Protocol.ElementInsert(_webApplication.Parent, new("Head", null, value));
            append.Wait();
        }
        public override Encoding Encoding { get; }
        public TextWriter Writer => this;
        public string Id => null;
    }
}