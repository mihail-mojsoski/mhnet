using System;
using System.IO;
using System.Text;
using MhNet.WebUI.UI.App;
using MhNet.WebUI.UI.Events;

namespace MhNet.WebUI.UI.Nodes
{
    public class Node<T> : TextWriter, INode where T : IWebApplication<T>, new()
    {
        public string Type { get; }
        private string _value;
        public string Value
        {
            set
            {
                _value = value;
                var task = _application.Protocol.ElementSet(_application.Parent, new("Node", Id, value));
                task.Wait();
            }
        }

        public string Id { get; }
        private readonly WebApplication<T> _application;
        // ReSharper disable once StaticMemberInGenericType
        private static ulong _nextId;

        private static string GenerateNewId()
        {
            return @"autonode" + _nextId++;
        }

        public override void Write(char value)
        {
            var append = _application.Protocol.ElementInsert(_application.Parent, new("Node", Id, value.ToString()));
            append.Wait();
        }

        public override void Write(string value)
        {
            var append = _application.Protocol.ElementInsert(_application.Parent, new("Node", Id, value));
            append.Wait();
        }
        public override Encoding Encoding { get; }
        public TextWriter Writer => this;
        string INode.Id => Id;

        public Node(WebApplication<T> application, string type, string value = "",INode parent = null,string id = null)
        {
            id ??= GenerateNewId();
            Encoding = Encoding.Default;
            _value = value;
            Id = id;
            Type = type;
            _application = application;
            AddTo(parent ?? application.Body);
        }

        private void AddTo(INode node)
        {
            node.Writer.Write($"<{Type} id=\"{Id}\">{_value}</{Type}>");
        }
        public string this[string key]
        {
            set => _application.SetAttribute(Id, key, value).Wait();
            get => _application.GetAttribute(Id, key).Result.Value;
        }

        public Action<object> this[Event @event]
        {
            get => _application.Events[new(Id, EventTarget.Node, @event)];
            set => _application.RegisterEvent(new NodeEventRequest(@event, Id), value).Wait();
        }
    }
}