protocol.RegisterDocumentEvent = function(data) {
    let req = JSON.parse(data);
    document.addEventListener(req['Event'], function(event) {
        event['EventInfo'] = {
            Event: req['Event'],
            Target: 'Document'
        };
        Send('EventCallback', stringifyEvent(event));
    });
}

protocol.SetNodeProperty = function(data) {
    let req = JSON.parse(data);
    let node = document.getElementById(req['Id']);
    node.setAttribute(req['Key'],req['Value']);
}

protocol.GetNodeProperty = function(data) {
    let req = JSON.parse(data);
    let node = document.getElementById(req['Id']);
    let value = node.getAttribute(req['Key']);
    Send('NodeProperty',JSON.stringify({Id:req['Id'], Key:req['Key'], Value:value}));
}

protocol.RegisterNodeEvent = function(data) {
    let req = JSON.parse(data);
    document.getElementById(req['Id']).addEventListener(req['Event'], function(event) {
        event['EventInfo'] = {
            Event: req['Event'],
            Target: 'Node',
            Id: req['Id']
        };
        Send('EventCallback', stringifyEvent(event));
    });
}
Send('Ready', null);