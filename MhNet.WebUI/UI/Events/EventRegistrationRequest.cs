using System;
using System.IO;
using MhNet.WebUI.WebProtocol.Server;

namespace MhNet.WebUI.UI.Events
{
    public abstract record EventRegistrationRequest : ProtocolData
    {
        public string Event { get; internal init; }

        public EventPoint ToJsEvent()
        {
            string id = null;
            
            if (this is NodeEventRequest nodeEvent)
            {
                id = nodeEvent.Id;
            }

            if (Enum.TryParse(Event, true, out Event eventType))
                return new(id, id == null ? EventTarget.Document : EventTarget.Node, eventType);
            throw new InvalidDataException();
        }

    }
}