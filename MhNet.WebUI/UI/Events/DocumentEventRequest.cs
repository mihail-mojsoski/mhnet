using System;
using Newtonsoft.Json;

namespace MhNet.WebUI.UI.Events
{
    [Serializable]
    public record DocumentEventRequest : EventRegistrationRequest
    {
        public DocumentEventRequest(Event @event)
        {
            Event = @event.ToString().ToLower();
        }
    }
}