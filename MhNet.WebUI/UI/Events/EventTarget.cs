namespace MhNet.WebUI.UI.Events
{
    public enum EventTarget
    {
        Document,
        Node
    }
}