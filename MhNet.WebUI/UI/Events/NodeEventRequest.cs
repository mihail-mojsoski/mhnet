using System;
using Newtonsoft.Json;

namespace MhNet.WebUI.UI.Events
{
    [Serializable]
    public record NodeEventRequest : EventRegistrationRequest
    {
        public string Id { get; }

        public NodeEventRequest(Event @event, string id)
        {
            Event = @event.ToString().ToLower();
            Id = id;
        }
    }
}