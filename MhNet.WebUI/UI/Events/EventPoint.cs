using System;

namespace MhNet.WebUI.UI.Events
{
    [Serializable]
    public record EventPoint(string Id, EventTarget Target, Event Type);
}