protocol.IsServerOk = function(data) {
    if (JSON.parse(data) !== true) {
        Report('Failed Connecting','Could not connect to the mhevent server!');
    } else {
        document.title = "Connected";
        console.log('Successfully connected to the server.');
    }
}
protocol.Error = function(data) {
    Report('Unhandled Exception', 'Unhandled exception: ' + data);
}
protocol.Eval = function(data) {
    eval(JSON.parse(data));
}
protocol.LoadScript = function(data) {
    LoadScript(JSON.parse(data));
}

protocol.ElementSet = function(data) {
    let req = JSON.parse(data);
    switch (req['Target']) {
        case 'Node':
            document.getElementById(req['Id']).innerHTML = req['Html'];
            break;
        case 'Head':
            document.head.innerHTML = req['Html'];
            break;
        case 'Body':
            document.body.innerHTML = req['Html'];
            break;
    }
};
protocol.ElementInsert = function(data) {
    let req = JSON.parse(data);
    switch (req['Target']) {
        case 'Node':
            document.getElementById(req['Id']).insertAdjacentHTML('beforeend', req['Html']);
            break;
        case 'Head':
            document.head.insertAdjacentHTML("beforeend", req['Html']);
            break;
        case 'Body':
            document.body.insertAdjacentHTML("beforeend", req['Html']);
            break;
    }
};