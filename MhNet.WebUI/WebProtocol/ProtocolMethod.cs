namespace MhNet.WebUI.WebProtocol
{
    public enum ProtocolMethod
    {
        IsServerOk,
        Error,
        Eval,
        LoadScript,
        ElementInsert,
        ElementSet
    }
}