using Newtonsoft.Json;

namespace MhNet.WebUI.WebProtocol.Server
{
    public record ProtocolData
    {
        protected ProtocolData()
        {
            _obj = null;
        }
        
        public ProtocolData(object obj)
        {
            _obj = obj;
        }

        private readonly object _obj;
        public string ToJsonString()
        {
            return JsonConvert.SerializeObject(_obj ?? this);
        }
    }
}