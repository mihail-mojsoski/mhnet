using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MhNet.WebUI.UI.App;

namespace MhNet.WebUI.WebProtocol.Server
{
    public interface IMethodProvider<T> where T : IWebApplication<T>, new()
    {
        Dictionary<string, Func<ProtocolManager<T>, ProtocolMessage, Task>> GetMethods();
        void Init(ProtocolServer<T> server);
    }
}