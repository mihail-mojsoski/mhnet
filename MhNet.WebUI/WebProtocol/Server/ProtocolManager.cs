using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;
using MhNet.WebUI.UI.App;

namespace MhNet.WebUI.WebProtocol.Server
{
    public class ProtocolManager<T> where T : IWebApplication<T>, new()
    {
        public bool IsRunning { get; set; } = true;
        private readonly WebSocket _socket;
        private readonly CancellationToken _token;
        public readonly T Instance;

        private readonly Dictionary<string, Func<ProtocolManager<T>, ProtocolMessage, Task>> _commands;
        public ProtocolManager(WebSocket socket, IMethodProvider<T> provider, CancellationToken token)
        {
            Instance = new();
            _token = token;
            _socket = socket;
            _commands = provider.GetMethods();
        }
        public async Task HandleException(Exception e)
        {
            IsRunning = false;
            await Send(ProtocolMethod.Error, new ProtocolData(e));
        }

        public async Task Send(ProtocolMessage message)
        {
            await _socket.SendEventMessage(message, _token);
        }
        public async Task Send<TFirst,TSecond>(TFirst method, TSecond data = null) where TSecond : ProtocolData
        {
            await _socket.SendEventMessage(new() {Command = method.ToString(), Data = data.ToJsonString()}, _token);
        }
        public async Task HandleMessage(ProtocolMessage message)
        {
            if (_commands.ContainsKey(message.Command))
            {
                await _commands[message.Command](this, message);
            }
        }

        public void Death()
        {
            Instance.Stop();
        }
    }
}