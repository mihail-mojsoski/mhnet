#nullable enable
namespace MhNet.WebUI.WebProtocol.Server
{
    public record TargetRequest(string Target, string? Id, string Html) : ProtocolData;
}