using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MhNet.WebUI.UI.App;

namespace MhNet.WebUI.WebProtocol.Server
{
    public abstract class Protocol<T> : IMethodProvider<T> where T : IWebApplication<T>, new()
    {
        protected readonly Dictionary<string, Func<ProtocolManager<T>, ProtocolMessage, Task>> Commands;
        protected Protocol(Dictionary<string, Func<ProtocolManager<T>, ProtocolMessage, Task>> commands)
        {
            commands.TryAdd(ProtocolMethod.IsServerOk.ToString(), IsServerOk);
            Commands = commands;
        }
        public Dictionary<string, Func<ProtocolManager<T>, ProtocolMessage, Task>> GetMethods() => Commands;
        public abstract void Init(ProtocolServer<T> server);

        protected abstract Task Bind(ProtocolManager<T> self);
        private async Task IsServerOk(ProtocolManager<T> self, ProtocolMessage message)
        {
            await self.Send(ProtocolMethod.IsServerOk,new ProtocolData(true));
            await Bind(self);
        }
    }
}