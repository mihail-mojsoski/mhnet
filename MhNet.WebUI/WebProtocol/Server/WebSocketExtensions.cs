using System;
using System.IO;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MhNet.WebUI.WebProtocol.Server
{
    public static class WebSocketExtensions
    {
        public static async Task<string> ReadMessageFromWebSocket(this WebSocket socket, CancellationToken token, int bufferSize=1024)
        {
            var buffer = new byte[bufferSize];
            var builder = new StringBuilder();
            WebSocketReceiveResult result;
            do
            {
                token.ThrowIfCancellationRequested();
                result = await socket.ReceiveAsync(buffer, token);
                if (result.MessageType != WebSocketMessageType.Text)
                {
                    throw new InvalidDataException();
                }
                builder.Append(Encoding.UTF8.GetString(new ReadOnlySpan<byte>(buffer, 0, result.Count)));
            }
            while (!result.EndOfMessage);
            return builder.ToString();
        }

        public static async Task<ProtocolMessage> ReadEventMessage(this WebSocket socket, CancellationToken token)
        {
            var message = await socket.ReadMessageFromWebSocket(token);
            return JsonConvert.DeserializeObject<ProtocolMessage>(message);
        }
        
        public static async Task SendEventMessage(this WebSocket socket, ProtocolMessage message, CancellationToken token)
        {
            await socket.WriteMessageToWebSocket(JsonConvert.SerializeObject(message), token);
        }
        
        public static async Task WriteMessageToWebSocket(this WebSocket socket, string message, CancellationToken token)
        {
            await socket.SendAsync(Encoding.UTF8.GetBytes(message), WebSocketMessageType.Text, true, token);
        }
    }
}