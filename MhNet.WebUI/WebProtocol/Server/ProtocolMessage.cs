using System;
using System.Diagnostics.CodeAnalysis;

namespace MhNet.WebUI.WebProtocol.Server
{
    [Serializable]
    public struct ProtocolMessage
    {
        public string Command;
        [AllowNull]
        public string Data;
    }
}