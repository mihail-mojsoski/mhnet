using System;
using System.Net;
using System.Threading.Tasks;
using MhNet.Common;
using MhNet.WebUI.UI.App;

namespace MhNet.WebUI.WebProtocol.Server
{
    public sealed partial class ProtocolServer<T> : IDisposable where T : IWebApplication<T>, new()
    {
        private HttpListener _listener;
        public readonly StreamRouter Router = new();
        private TaskChecker _tasks;
        private readonly IMethodProvider<T> _protocol;

        public ProtocolServer(IMethodProvider<T> protocol)
        {
            _protocol = protocol;
        }
        private static string GetContentType(string extension, string fallback = "text/html")
        {
            lock (MimeTypes.FallbackMimeType)
            {
                MimeTypes.FallbackMimeType = fallback;
                return MimeTypes.GetMimeType(extension);
            }
        }
        public async Task Start(string prefix)
        {
            Router.FromAssembly(typeof(ProtocolServer<T>).Assembly,"/");
            _tasks = new(e => Error.Report(nameof(TaskChecker), e));
            _protocol.Init(this);
            _listener = new();
            _listener.Prefixes.Add(prefix);
            _listener.Start();
            while (_listener.IsListening)
            {
                var context = await _listener.GetContextAsync();
                _tasks.Run(async _ => await SendRoutedPage(context, _tasks.TokenSource.Token));
            }
        }

        public void Stop()
        {
            _listener?.Stop();
        }

        public void Dispose()
        {
            Router.Dispose();
            _tasks.Dispose();
        }
    }
}