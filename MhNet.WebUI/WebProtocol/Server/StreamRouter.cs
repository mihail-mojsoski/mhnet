using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace MhNet.WebUI.WebProtocol.Server
{
    public class StreamRouter : IDisposable
    {
        private readonly Dictionary<string, Stream> _streams = new();
        private readonly Func<string,Stream> _notFound;

        public void Read(string path, Action<Stream> action)
        {
            var stream = this[path];
            lock (stream)
            {
                if (stream.CanSeek)
                {
                    stream.Position = 0;
                }

                action(stream);
            }
        }
        public void ReadString(string path, Action<StreamReader> action)
        {
            Read(path, stream =>
            {
                using var reader = new StreamReader(stream);
                action(reader);
            });
        }
        public void FromAssembly(Assembly asm, string path)
        {
            foreach (var resource in asm.GetManifestResourceNames())
            {
                StringBuilder builder = new(resource);
                builder.Replace(asm.GetName().Name + '.', path);
                builder.Replace(".", "/");
                
                // Save the extension
                var loc = builder.ToString().LastIndexOf('/');
                builder[loc]='.';
                
                this[builder.ToString()] = asm.GetManifestResourceStream(resource);
            }
        }
        private void Unmap(string path)
        {
            if (!_streams.ContainsKey(path)) return;
            
            _streams[path].Dispose();
            _streams.Remove(path);
        }

        public StreamRouter(Func<string, Stream> notFound = null)
        {
            _notFound = notFound ?? (path => throw new FileNotFoundException("The route was not found.", path));
        }
        private void Map(string path, Stream stream)
        {
            Unmap(path);
            _streams.Add(path, stream);
        }

        private Stream Route(string path)
        {
            try
            {
                return _streams[path];
            }
            catch
            {
                return _notFound?.Invoke(path);
            }
        }
        public Stream this[string index]
        {
            get => Route(index);
            set => Map(index, value);
        }

        public void Dispose()
        {
            foreach (var v in _streams)
            {
                v.Value?.Dispose();
            }
        }
    }
}