using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MhNet.Common;
using MhNet.WebUI.UI.App;

namespace MhNet.WebUI.WebProtocol.Server
{
    public sealed partial class ProtocolServer<T> where T : IWebApplication<T>, new()
    {
        private async Task SendRoutedPage(HttpListenerContext context, CancellationToken? token)
        {
            try
            {
                var path = context.Request.Url?.AbsolutePath;
                if (context.Request.IsWebSocketRequest)
                { 
                    await SocketConnection(context, token.GetValueOrDefault());
                }
                else
                {
                    if (path == "/")
                    {
                        path = "/Base/Main.html";
                    }

                    context.Response.ContentType = GetContentType(path);
                    if (context.Response.ContentType?.StartsWith("text") == true)
                    {
                        context.Response.ContentType += "; charset=utf-8";
                    }

                    SendCachedPage(context, path);
                }
            }
            catch (Exception e)
            {
                try
                {
                    SendErrorPage(context, e);
                }
                catch(Exception ex)
                {
                    if (ex.ToString().Contains("Broken pipe"))
                    {
                        return;
                    }
                    Error.Report(nameof(SendErrorPage), ex);
                    Error.Report(nameof(SendRoutedPage), e);
                }
            }
            finally
            {
                context.Response.Close();
            }
        }

        private void SendFormattedPage(HttpListenerContext context, string path, Func<string, string> format)
        {
            var writer = new StreamWriter(context.Response.OutputStream);
            Router.ReadString(path, reader =>
            {
                writer.Write(format(reader.ReadToEnd()));
                writer.Flush();
            }); 
        }
        private void SendCachedPage(HttpListenerContext context, string path)
        {
            Router.Read(path,stream =>
            {
                context.Response.ContentLength64 = stream.Length;
                stream.CopyTo(context.Response.OutputStream);
            });
        }
        private void SendErrorPage(HttpListenerContext context, Exception exception)
        {
            try
            {
                const string errorPage = "/Base/Error.html";
                context.Response.ContentType = "text/html";
                
                if (exception.GetType() == typeof(FileNotFoundException))
                {
                    context.Response.StatusCode = 404;
                    context.Response.StatusDescription = "Not found";
                    SendFormattedPage(context, errorPage, str => str
                        .Replace("{0}", "Not Found")
                        .Replace("{1}", "The instance you have requested was not found."));
                }
                else
                {
                    context.Response.StatusCode = 500;
                    context.Response.StatusDescription = "Internal server error";
                    SendFormattedPage(context, errorPage, str => str
                        .Replace("{0}", "Internal Server Error")
                        .Replace("{1}", $"<p>{exception.Message}</p>"));
                }
            }
            catch(Exception e)
            {
                context.Response.StatusCode = 500;
                context.Response.ContentType = "text/plain";
                var simple =
                    $"Failed generating the error page...{Environment.NewLine}{exception}{Environment.NewLine}{new string('-', 50)}{Environment.NewLine}{e}";
                context.Response.OutputStream.Write(Encoding.Default.GetBytes(simple) );
                context.Response.OutputStream.Flush();
            }
        }

    }
}