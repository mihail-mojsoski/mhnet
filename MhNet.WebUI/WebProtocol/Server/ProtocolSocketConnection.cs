using System;
using System.Net;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;
using MhNet.WebUI.UI.App;

namespace MhNet.WebUI.WebProtocol.Server
{
    public sealed partial class ProtocolServer<T> where T : IWebApplication<T>, new()
    {
        private async Task SocketConnection(HttpListenerContext context, CancellationToken token)
        {
            var socketContext = await context.AcceptWebSocketAsync("mh");
            var events = new ProtocolManager<T>(socketContext.WebSocket, _protocol, token);
            while (events.IsRunning && (socketContext.WebSocket.CloseStatus == null || socketContext.WebSocket.CloseStatus == WebSocketCloseStatus.Empty))
            {
                token.ThrowIfCancellationRequested();
                try
                {
                    await events.HandleMessage(await socketContext.WebSocket.ReadEventMessage(token));
                }
                catch (Exception e)
                {
                    await events.HandleException(e);
                }
            }
            events.Death();
            await socketContext.WebSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Stop", token);
        }

    }
}