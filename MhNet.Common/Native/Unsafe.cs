using System;

namespace MhNet.Common.Native
{
    public static class Unsafe
    {
        /// <summary>
        /// Fix a variable in memory and get it's pointer, then pass that pointer to a function
        /// </summary>
        /// <param name="type">The variable</param>
        /// <param name="action">The function</param>
        /// <typeparam name="TPointer">The variable's type</typeparam>
        /// <typeparam name="TReturn">The return value of the function</typeparam>
        /// <returns>The result of the function</returns>
        public static unsafe TReturn AsPointer<TPointer, TReturn>(this ref TPointer type,
            Func<IntPtr, TReturn> action) where TPointer : unmanaged
        {
            fixed (TPointer* ptr = &type)
            {
                return action((IntPtr) ptr);
            }
        }
    }
}