using System;
using System.Diagnostics;

namespace MhNet.Common.Native
{
    public static partial class Linux
    {
        /// <summary>
        /// A pointer that has the value 0
        /// </summary>
        public static readonly IntPtr Null = IntPtr.Zero;
        
        /// <summary>
        /// Create a new process
        /// </summary>
        /// <returns>The new process</returns>
        public static Process Fork()
        {
            var pid = fork();
            return pid == 0 ? Process.GetCurrentProcess() : Process.GetProcessById(pid);
        }

        /// <summary>
        /// Cause normal process termination
        /// </summary>
        /// <param name="status">The status that gets returned to the parent process</param>
        public static void Exit(int status)
        {
            exit(status);
        }

        /// <summary>
        /// Wait for process to change state
        /// </summary>
        /// <param name="ptr">The status that was returned by the child process gets written to this pointer</param>
        /// <returns>The child process ID</returns>
        public static int Wait(IntPtr ptr)
        {
            return wait(ptr);
        }
    }
}