using System;
using System.Runtime.InteropServices;

namespace MhNet.Common.Native
{
    public static partial class Linux
    {
        [DllImport("libc")]
        private static extern int wait(IntPtr status);

        [DllImport("libc")]
        private static extern int fork();

        [DllImport("libc")]
        private static extern void exit(int status);

    }
}