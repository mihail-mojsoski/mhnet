using System.Threading.Tasks;

namespace MhNet.Common
{
    public class Request
    {
        private object _result;
        private bool _complete;

        public async Task<T> GetResult<T>(int delay = 10) where T : class
        {
            while (!_complete)
                await Task.Delay(delay);
            return _result as T;
        }
        public void Complete(object result)
        {
            _complete = true;
            _result = result;
        }
    }
}