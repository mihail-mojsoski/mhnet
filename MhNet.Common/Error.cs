﻿using System;
using System.Net;

namespace MhNet.Common
{
    public static class Error
    {
        public static Action<string, Exception> Report { get; set; } = (location, exception)
            =>
        {
            if (exception is HttpListenerException)
            {
                return;
            }
            Console.Error.WriteLine($"Encountered a problem at `{location}`: {exception}");
        };

        public static bool ReportIf(bool condition, string location, Exception exception, bool exit)
        {
            if (!condition) return true;
            Report(location, exception);
            if (exit) Environment.Exit(1);
            return false;
        }

        public static T ReportIfFails<T>(Func<T> action, string location, bool exit) where T : class
        {
            try
            {
                return action();
            }
            catch (Exception e)
            {
                Report(location, e);
                if (exit) Environment.Exit(1);
                return null;
            }
        }
    }
}