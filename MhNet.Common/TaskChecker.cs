using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MhNet.Common
{
    public class TaskChecker : IDisposable
    {
        private readonly List<Task> _tasks;
        private readonly Task _checker;
        private readonly Action<Exception> _onException;
        public readonly CancellationTokenSource TokenSource;

        public TaskChecker(Action<Exception> onException)
        {
            TokenSource = new();
            _tasks = new();
            _onException = onException;
            _checker = Task.Run(() => CheckerThread(), TokenSource.Token);
        }

        public void Run(Action<CancellationToken> action)
        {
            var task = Task.Run(() => action(TokenSource.Token), TokenSource.Token);
            _tasks.Add(task);
        }
        private void CheckerThread()
        {
            var deleteQueue = new List<Task>();
            while (_tasks != null)
            {
                foreach (var task in _tasks.Where(task => task.IsCompleted))
                {
                    deleteQueue.Add(task);
                    if (task.IsFaulted)
                    {
                        
                        _onException(task.Exception);
                    }
                }

                foreach (var task in deleteQueue)
                {
                    task?.Dispose();
                    _tasks.Remove(task);
                }

                deleteQueue.Clear();
                TokenSource.Token.ThrowIfCancellationRequested();
                Thread.Sleep(100);
            }
        }

        private void Free()
        {
            TokenSource.Cancel();
            foreach (var v in _tasks)
            {
                try
                {
                    v?.Dispose();
                }
                catch
                {
                    // ignored
                }
            }

            try
            {
                _checker?.Dispose();
            }
            catch
            {
                // ignored
            }
        }
        
        ~TaskChecker()
        {
            Free();
        }
        
        public void Dispose()
        {
            Free();
            GC.SuppressFinalize(this);
        }
    }
}