using System;
using System.Threading.Tasks;
using MhNet.WebUI.UI.App;
using MhNet.WebUI.UI.Events;

namespace Test
{
    public class Application : WebApplication<Application>
    {
        protected override async Task Request()
        {
            var style = await CreateNode("link", parent: Head);
            style["rel"] = "stylesheet";
            style["href"] = "/Button.css";
            
            var exitButton = await CreateNode("button", "X");
            exitButton["class"] = "exit-button";
            exitButton[Event.Click] = _ => Environment.Exit(0);
            exitButton[Event.MouseOut] = _ => exitButton["class"] = "exit-button";
            exitButton[Event.MouseOver] = _ => exitButton["class"] = "exit-button exit-button-active";
            
            var text = await CreateNode("textarea");
            text["placeholder"] = "Type something in here!";
        }
    }
}