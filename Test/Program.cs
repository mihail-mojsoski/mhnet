﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using MhNet.WebUI.UI.App;

namespace Test
{
    public static partial class Program
    {
        public static readonly Dictionary<string, string> Options = new()
        {
            {"Host", "127.0.0.1:6969"},
            {"Start", "Default"}
        };
        public static async Task Main()
        {
            if (File.Exists("Server.conf"))
            {
                var lines = await File.ReadAllLinesAsync("Server.conf");
                foreach (var line in lines)
                {
                    var equal = line.IndexOf('=');
                    var key = line[..equal];
                    var value = line[(equal + 1)..];
                    Options[key] = value;
                }
            }
            var application = WebApplicationProtocol<Application>.Start($"http://{Options["Host"]}/");
            await Task.Delay(100);
            await Starters[Options["Start"]](Options["Host"]);
            application.Wait();
        }
    }
}