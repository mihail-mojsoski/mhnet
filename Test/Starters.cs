using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using MhNet.Common;

namespace Test
{
    public static partial class Program
    {
        private static Process _browser;

        private static readonly Dictionary<string, Func<string, Task>> Starters = new()
        {
            {"LinuxChrome", LinuxChrome},
            {"LinuxFirefox", LinuxFirefox},
            {"Default", Default}
        };
        private static Task LinuxChrome(string host)
        {
            if (!Error.ReportIf(Environment.OSVersion.Platform != PlatformID.Unix, "StartChrome",
                new PlatformNotSupportedException(), false)) return Task.CompletedTask;
            _browser = Sh($"google-chrome-stable --kiosk --host-rules='MAP * {host}' {host}", out _, out _, true);
            return _browser?.WaitForExitAsync();
        }
        private static Task LinuxFirefox(string host)
        {
            if (!Error.ReportIf(Environment.OSVersion.Platform != PlatformID.Unix, "StartFirefox",
                new PlatformNotSupportedException(), false)) return Task.CompletedTask;

            _browser = Sh($"firefox {host}", out _, out _ ,true);
            return _browser?.WaitForExitAsync();
        }

        private static Process Sh(string command,out StreamReader output, out StreamReader error,bool mute = false)
        {
            var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "sh",
                    Arguments = $"-c \"{command.Replace("\"", "\\\"")} {(mute ? " >/dev/null 2>&1" : "")}\"",
                    RedirectStandardOutput = !mute,
                    RedirectStandardError =  !mute
                }
            };
            process.Start();
            output = !mute ? process.StandardOutput : null;
            error = !mute ? process.StandardError : null;
            return process;
        }

        private static string ShExec(string command)
        {
            var process = Sh(command, out var reader, out var error);
            process.WaitForExit();
            return reader.ReadToEnd() + error.ReadToEnd();
        }

        private static bool IsValid(string str)
        {
            return !ShExec($"which {str}").Contains($"no {str}");
        }
        private static Task Default(string host)
        {
            if (Environment.OSVersion.Platform == PlatformID.Unix)
            {
                var chrome = IsValid("google-chrome-stable");
                var firefox = IsValid("firefox");

                switch(chrome)
                {
                    case true when firefox:
#if DEBUG
                        return LinuxFirefox(host);
#else
                        return LinuxChrome(host);
#endif
                    case true:
                        return LinuxChrome(host);
                }

                if (firefox)
                {
                    return LinuxFirefox(host);
                }
            }

            Console.WriteLine($"Application is running on http://{host}/");
            return Task.CompletedTask;
        }
    }
}