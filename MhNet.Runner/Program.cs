﻿using MhNet.Common;

namespace MhNet.Runner
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            Error.ReportIf(args.Length < 1, "Main", new("Not enough arguments."), true);
            new Run(args[0], true).Execute(args[1..]);
        }
    }
}