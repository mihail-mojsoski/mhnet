#nullable enable
using System;
using System.Diagnostics;
using System.Reflection;
using MhNet.Common;
using MhNet.Common.Native;

namespace MhNet.Runner
{
    public class Run
    {
        public Run(string assembly, bool exit = false)
        {
            Exit = exit;
            Assembly = Error.ReportIfFails(() => Assembly.Load(assembly), "Run.ctor", Exit);
            Error.ReportIf(Assembly is null, "Run.ctor", new("No assembly is loaded."), Exit);
        }

        public bool Exit { get; }
        public Assembly? Assembly { get; }

        // ReSharper disable once UnusedMethodReturnValue.Global
        public object? Execute(string[] args)
        {
            Error.ReportIf(Assembly?.EntryPoint is null, "Run.Execute", new("Assembly does not have an entry point."),
                Exit);
            return Error.ReportIfFails(() => Assembly?.EntryPoint?.Invoke(null, new object[] {args}), "Run.Execute",
                Exit);
        }

        public Process ExecuteFork(string[] args)
        {
            var fork = Linux.Fork();
            if (fork.Id != Environment.ProcessId) return fork;

            Execute(args);
            Environment.Exit(0);
            return null;
        }
    }
}